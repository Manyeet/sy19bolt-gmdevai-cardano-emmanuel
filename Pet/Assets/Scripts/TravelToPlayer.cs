﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelToPlayer : MonoBehaviour
{
    public Transform Player;
    public float speed;
    public float rotationSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lookAtPlayer = new Vector3(Player.position.x, this.transform.position.y, Player.position.z);

        Vector3 direction = lookAtPlayer - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

        if (Vector3.Distance(lookAtPlayer, transform.position) > 1)
        {
            transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }
}
