﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    public GameObject player;
    Vector3 playerLocation;
    GameObject[] agents;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject ai in agents)
        {
            playerLocation = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
            ai.GetComponent<AIControl>().agent.SetDestination(playerLocation);
        }
    }
}